package list

/*
 * List data structure with a slice as internal structure
 *
 * Author: Michael Baldwin
 *
 * References:
 * - http://blog.golang.org/go-slices-usage-and-internals
 * - http://stackoverflow.com/questions/21326109/why-are-lists-used-infrequently-in-golang
 */

type List struct {
    items []interface{}
    size int
}

/*
 * Creates and returns an empty list
 */
func New() (l *List) {
    l = new(List)

    // create a slice with zero items to begin with
    l.items = make([]interface{}, 0)

    return l
}

/*
 * Adds data onto the end of the list
 */
func (l *List) Append(data interface{}) {

    // append to list
    l.items = append(l.items, data)

    // increment size
    l.size++
}

/*
 * Retrieves data from the list at the specified index
 */
func (l *List) Get(index int) interface{} {

    // retrieve from list at index
    data := l.items[index]

    // return data
    return data
}

/*
 * Returns the current size of the list
 */
func (l *List) Size() int {
    return l.size
}
