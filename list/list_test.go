package list

import (
    "testing"
)

/*
* Package: List Data Structure - Testing
* Author: Michael Baldwin
*
* GoLang Learning:
* - http://stackoverflow.com/questions/21326109/why-are-lists-used-infrequently-in-golang
*/

/*
* Test creating a new list
*/
func TestNew(t *testing.T) {

    // create new list
    l := New()

    // items should not be nil since created a slice underneath
    if l.items == nil {
        t.Errorf("New(), list.items == %q, expected %q", nil, "!nil")
    }

    // size should be zero
    if l.Size() != 0 {
        t.Errorf("New(), list.Size() == %q, expected %q", l.Size(), 0)
    }
}

/*
 * Test appending data to the end of a list
 */
func TestAppend(t *testing.T) {

    // create new list
    l := New()

    // append data1 to list
    data1 := "test1"
    l.Append(data1)

    if l.items[0] != data1 {
        t.Errorf("Append(), l.items[%q] == %q, expected %q", 0, l.items[0], data1)
    }

    // append data2 to list
    data2 := "test2"
    l.Append(data2)

    if l.items[1] != data2 {
        t.Errorf("Append(), l.items[%q] == %q, expected %q", 1, l.items[1], data2)
    }

    // append data3 to list
    data3 := "test3"
    l.Append(data3)

    if l.items[2] != data3 {
        t.Errorf("Append(), l.items[%q] == %q, expected %q", 2, l.items[2], data3)
    }
}

/*
 * Test getting data from the specified index of a list
 */
func TestGet(t *testing.T) {

    // create new list
    l := New()

    // append data to list
    data := "testData"
    l.Append(data)
    l.Append(data)
    l.Append(data)

    // check that getting data is accurate
    for i := 0; i < l.Size(); i++ {
        if l.Get(i) != data {
            t.Errorf("Get(), list.Get(%q) == %q, expected %q", i, l.Get(i), data)
        }
    }
}

/*
 * Test returning the size of a list
 */
func TestSize(t *testing.T) {

    // create new list
    l := New()

    // size should be zero
    if l.Size() != 0 {
        t.Errorf("Size(), list.Size() == %q, expected %q", l.Size(), 0)
    }

    // check that size is being tracked properly
    const limit = 1000
    for i := 1; i <= limit; i++ {
        l.Append("testdata")
        if l.Size() != i {
            t.Errorf("Size(), list.Size() == %q, expected %q", l.Size(), i)
        }
     }
}
