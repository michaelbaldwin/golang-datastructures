# golang-datastructures
Data structures for the GoLang Programming Language


## queue
- Enqueue(data interface{})
- Dequeue() interface{}
- IsEmtpty() bool
- Peek() interface{}
- Size() int


## list
- Append(data interface{})
- Get(index int) interface{}
- Size() int
