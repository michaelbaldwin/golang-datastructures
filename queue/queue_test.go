package queue

import (
    "testing"
    "reflect"
)

/*
* Package: Queue Data Structure - Testing
* Author: Michael Baldwin
*
* GoLang Learning:
* - https://www.golang-book.com/books/intro/12
* - http://stackoverflow.com/questions/15311969/checking-the-equality-of-two-slices
*/

/*
* Test creating new queue
*/
func TestNew(t *testing.T) {

    // create new queue
    q := New()

    // first member should be nil
    if q.first != nil {
        t.Errorf("New(), queue.first == %q, expected %q", q.first, nil)
    }

    // last member should be nil
    if q.last != nil {
        t.Errorf("New(), queue.last == %q, expected %q", q.last, nil)
    }

    // size should be zero
    if q.size != 0 {
        t.Errorf("New(), queue.size == %q, expected %q", q.size, 0)
    }
}

/*
* Test enqueue function
*/
func TestEnqueue(t *testing.T) {

    // create queue
    q := New()

    // enqueue first item
    first := "hello world"
    q.Enqueue(first)

    // first and last members should point to same node since only one item in
    // queue
    if q.first != q.last {
        t.Errorf("Enqueue(), queue.first != queue.last, expected queue.first == queue.last")
    }

    // size should be 1
    if q.size != 1 {
        t.Errorf("Enqueue(), queue.size == %q, expected %q", q.size, 1)
    }

    // enqueue second item
    second := 1.2
    q.Enqueue(second)

    // first and last members should NOT point to same node since more than
    // one item in queue now
    if q.first == q.last {
        t.Errorf("Enqueue(), queue.first == queue.last, expected queue.first != queue.last")
    }

    // size should be 2
    if q.size != 2 {
        t.Errorf("Enqueue(), queue.size == %q, expected %q", q.size, 2)
    }

    // enqueue third item
    third := []int{1, 2, 3}
    q.Enqueue(third)

    // size should be 3
    if q.size != 3 {
        t.Errorf("Enqueue(), queue.size == %q, expected %q", q.size, 3)
    }
}

/*
* Test dequeue function
*/
func TestDequeue(t *testing.T) {

    // create queue
    q := New()

    // enqueue three items
    first := "hello world"
    second := 1.2
    third := []int{1, 2, 3}
    q.Enqueue(first)
    q.Enqueue(second)
    q.Enqueue(third)

    // dequeue first item
    firstItem := q.Dequeue()

    // data should match
    if firstItem != first {
        t.Errorf("Dequeue(), queue.Dequeue == %q, expected %q", firstItem, first)
    }

    // first and last members should point to different nodes since more than
    // one item in queue
    if q.first == q.last {
        t.Errorf("Dequeue(), queue.first == queue.last, expected queue.first != queue.last")
    }

    // size of queue should now be 2
    if q.size != 2 {
        t.Errorf("Dequeue(), queue.size == %q, expected %q", q.size, 2)
    }

    // dequeue second item
    secondItem := q.Dequeue()

    // data should match
    if secondItem != second {
        t.Errorf("Dequeue(), queue.Dequeue == %q, expected %q", secondItem, second)
    }

    // first and last members should point to same node since only one item in
    // queue
    if q.first != q.last {
        t.Errorf("Dequeue(), queue.first != queue.last, expected queue.first == queue.last")
    }

    // size of queue should now be 1
    if q.size != 1 {
        t.Errorf("Dequeue(), queue.size == %q, expected %q", q.size, 1)
    }

    // dequeue third item
    thirdItem := q.Dequeue()

    // data should match
    if !reflect.DeepEqual(thirdItem, third) {
        t.Errorf("Dequeue(), queue.Dequeue == %q, expected %q", thirdItem, third)
    }

    // first and last members should point to nil since nothing in queue now
    if q.first != nil {
        t.Errorf("Dequeue(), queue.first == %q, expected %q", q.first, nil)
    }

    if q.last != nil {
        t.Errorf("Dequeue(), queue.last == %q, expected %q", q.last, nil)
    }

    // size of queue should now be 0
    if q.size != 0 {
        t.Errorf("Dequeue(), queue.size == %q, expected %q", q.size, 0)
    }

}

/*
* Test isEmpty function
*/
func TestIsEmpty(t *testing.T) {

    // create new queue
    q := New()

    // should be empty
    if !q.IsEmpty() {
        t.Errorf("IsEmpty(), q.IsEmpty == %q, expected %q", q.IsEmpty(), true)
    }

    // enqueue data
    q.Enqueue("test")

    // should not be empty
    if q.IsEmpty() {
        t.Errorf("IsEmpty(), q.IsEmpty == %q, expected %q", q.IsEmpty(), false)
    }

    // dequeue data
    q.Dequeue()

    // should be empty
    if !q.IsEmpty() {
        t.Errorf("IsEmpty(), q.IsEmpty == %q, expected %q", q.IsEmpty(), true)
    }
}


/*
* Test Peek function
*/
func TestPeek(t *testing.T) {

    // create new queue
    q := New()

    // enqueue data1
    data1 := "test1"
    q.Enqueue(data1)

    // peek should return data1
    if q.Peek() != data1 {
        t.Errorf("Peek(), q.Peek() != %q, expected q.Peek() == %q", q.Peek(), q.Peek())
    }

    // enqueue data2
    data2 := "test2"
    q.Enqueue(data2)

    // peek should still return data1
    if q.Peek() != data1 {
        t.Errorf("Peek(), q.Peek() != %q, expected q.Peek() == %q", q.Peek(), q.Peek())
    }

    // dequeue data1
    q.Dequeue()

    // peek should return data2
    if q.Peek() != data2 {
        t.Errorf("Peek(), q.Peek() != %q, expected q.Peek() == %q", q.Peek(), q.Peek())
    }
}


/*
* Test Size function
*/
func TestSize(t *testing.T) {

    // create new queue
    q := New()

    // should be empty
    if q.Size() != 0 {
        t.Errorf("Size(), q.size == %q, expected %q", q.Size(), 0)
    }

    // enqueue data
    q.Enqueue("test1")

    // should have one item
    if q.Size() != 1 {
        t.Errorf("Size(), q.size == %q, expected %q", q.Size(), 1)
    }

    // enqueue data
    q.Enqueue("test2")

    // should have two items
    if q.Size() != 2 {
        t.Errorf("Size(), q.size == %q, expected %q", q.Size(), 2)
    }

    // dequeue data
    q.Dequeue()

    // should have one item
    if q.Size() != 1 {
        t.Errorf("Size(), q.size == %q, expected %q", q.Size(), 1)
    }

    // dequeue data
    q.Dequeue()

    // should be empty
    if q.Size() != 0 {
        t.Errorf("Size(), q.size == %q, expected %q", q.Size(), 0)
    }
}
