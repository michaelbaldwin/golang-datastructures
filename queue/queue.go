package queue

/*
 * Author: Michael Baldwin
 *
 * Operations:
 *
 * New() *Queue
 * Enqueue(interface{})
 * Dequeue() interface{}
 * IsEmpty() bool
 * Peek() interface{}
 * Size() int
 *
 * References:
 * http://introcs.cs.princeton.edu/java/43stack/Queue.java.html
 * https://www.youtube.com/watch?v=A5_XdiK4J8A
 *
 * GoLang Learning:
 *
 * - when import a package "foo/bar" then the other package can use functions
 * and structs made available like "bar.func()"
 * https://golang.org/ref/spec#Import_declarations
 *
 * - functions must begin with a Capital in order to be used when imported
 * https://golang.org/ref/spec#Exported_identifiers
 *
 * - the 'interface{}' type represents any type
 * https://golang.org/ref/spec#Interface_types
 *
 * - when declaring function return values, type comes after variable name
 * https://golang.org/ref/spec#Function_declarations
 *
 * - when using the 'new' keyword to initialize a type, a struct's fields are
 * all intialized to zero types, like 0, nil, etc.
 * https://golang.org/ref/spec#The_zero_value
 */

import (
    "fmt"
)

/*
 * Queue implemented with a singly linked list
 */
type Queue struct {
    first *Node
    last *Node
    size int
}

/*
 * Node for a singly linked list
 */
type Node struct {
    data interface{}
    next *Node
}

/*
 * Creates and returns an empty queue
 */
func New() (q *Queue) {
    return new(Queue)
}

/*
 * Add an element to the end of the queue.
 */
func (q *Queue) Enqueue(data interface{}) {
    temp := new(Node)
    temp.data = data
    if q.IsEmpty() {
        q.first = temp
        q.last = temp
    } else {
        q.last.next = temp
        q.last = temp
    }
    q.size++
}

/*
 * Remove and return the first element in the queue.
 */
func (q *Queue) Dequeue() interface{} {
    if q.IsEmpty() {
        panic(fmt.Sprintf("Queue underflow"))
    }
    tempData := q.first.data
    q.first = q.first.next
    q.size--
    if q.IsEmpty() {
        q.last = nil
    }
    return tempData
}

/*
 * Check if the queue is empty.
 */
func (q *Queue) IsEmpty() bool {
    return q.first == nil
}

/*
 * Return (without removing) the first element in the queue.
 */
func (q *Queue) Peek() interface{} {
    if q.IsEmpty() {
        panic(fmt.Sprintf("Queue underflow"))
    }
    return q.first.data
}

/*
 * Get the size of the queue.
 */
func (q *Queue) Size() int {
    return q.size
}
